const fetch = require('node-fetch');
const FormData = require('form-data');

const baseServer = process.env.STRAPI_BASE_SERVER || "http://localhost:1338"

async function downloadToBlob(targetUrl) {
    const res = await fetch(targetUrl, {method: 'GET'});
    const blob = await res.blob();
    const match = targetUrl.match(/.+\/(.+?)($|\?).+/);
    if (match?.length > 1) {
        const filename = match[1];
        return {blob, filename};
    } else {
        const match2 = targetUrl.match(/.+\/(.+)/)
        if (match2?.length > 1) {
            const filename = match2[1];
            return {blob, filename};
        } else {
            throw new Error('Could not extract file name from URL');
        }
    }
}

async function uploadFile(form) {
    return await fetch(`${baseServer}/upload`, {
        method: 'POST',
        body: form
    });
}

function extractMimeType(targetUrl, type) {
    if(targetUrl.indexOf('.mp3') > -1) {
        return 'audio/mp3';
    }
    if(targetUrl.indexOf('.mp4') > -1) {
        return 'video/mp4';
    }
    const match = type.match(/(.+?)($|(;.+))/);
    if(match && match.length > 1) {
        console.log('Extract mime type', match[1]);
        return match[1];
    }
    return 'image/jpeg';
}

async function downloadFileToMediaFolder(targetUrl) {
    const {blob, filename} = await downloadToBlob(targetUrl);
    const buffer = await blob.arrayBuffer();
    const form = new FormData();
    form.append('files', Buffer.from(buffer), {
        contentType: extractMimeType(targetUrl, blob.type),
        name: 'file',
        filename: filename
    });
    const res = await uploadFile(form);
    return await res.json();
}

module.exports = {
    downloadFileToMediaFolder,
    baseServer
};

// Simple test
downloadFileToMediaFolder(`http://brahmakumaris.org/media/lm/101_breathe_and_relax.mp3?pageId=263`)
    .then(res => {
        console.log('json', res);
    });



