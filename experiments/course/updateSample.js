import fetch from 'node-fetch';

const sampleCourse = {"name": "Sample Course Changed", "summary": "Sample to delete", "sections": [7]};

fetch('http://localhost:1337/courses/4', {method: 'PUT', body: JSON.stringify(sampleCourse)})
    .then(res => res.json()) // expecting a json response
    .then(json => console.log(json))
    .catch(res => {
        console.error(res);
    });