import fetch from 'node-fetch';

fetch('http://localhost:1337/sections/5', {method: 'DELETE'})
    .then(res => res.json()) // expecting a json response
    .then(json => console.log(json))
    .catch(res => {
        console.error(res);
    });