const fetch = require('node-fetch');

async function deleteMediaFile(id) {
    return await fetch(`http://localhost:1337/upload/files/${id}`, {
        method: 'DELETE'
    });
}

async function listMediaFiles() {
    return await fetch(`http://localhost:1337/upload/files?_limit=1000&_start=0&_sort=updated_at:DESC`, {
        method: 'GET'
    });
}

listMediaFiles()
    .then(res => res.json())
    .then(json => json.map(o => o.id))
    .then(async ids => {
        const res = await ids.map(async id => {
            return await deleteMediaFile(id);
        });
        console.log(res)
    });



