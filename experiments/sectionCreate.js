import fetch from 'node-fetch';

const sampleSection = {
    "name": "Test Section",
    "accessible": true
};

fetch('http://localhost:1337/sections', {method: 'POST', body: JSON.stringify(sampleSection)})
    .then(res => res.json()) // expecting a json response
    .then(json => console.log(json))
    .catch(res => {
        console.error(res);
    });