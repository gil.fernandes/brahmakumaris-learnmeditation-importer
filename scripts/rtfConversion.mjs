import { exec } from 'child_process'
import path from 'path'


export default async function convertToPlain(file) {
    file = path.resolve(file)
    return new Promise((resolve, reject) => {
        let result = '';
        let errResult = '';

        const cmd = `python ./python/extract_rtf_text.py "${file}"`
        console.log('cmd', cmd)

        let child = exec(cmd);

        child.stdout.on('data', function(data) {
            result += data;
        });

        child.stderr.on('data', function(data) {
            errResult += data;
        });

        child.on('close', function() {
            if(!!errResult) {
                reject(errResult)
            } else {
                resolve(result);
            }
        });
    })
}