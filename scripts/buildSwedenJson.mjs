import fs from 'fs'
import path from 'path'
import reader from 'any-text'
import convertToPlain from './rtfConversion.mjs'

if (process.argv.length < 4) {
  console.error('Please specify the source and target directories.')
  process.exit(1)
}

const COURSE = {
  NAME: 'Lär dig meditera - Online',
  SUMMARY: 'Lär dig meditera',
}

const MODULE_TYPE = {
  QUIZ: 'quiz',
  PAGE: 'page',
}

const sourceDir = process.argv[2]
const targetDir = process.argv[3]
const sections = fs.readdirSync(sourceDir)

function extractModuleNumber (m) {
  const name = m.pageName ?? m.quizName
  return name.replace(/Sida\s+?(\d+).+/, '$1')
}

function sortCourseModuleList (courseModuleList) {
  return courseModuleList.sort((m1, m2) => {
    const n1 = extractModuleNumber(m1)
    const n2 = extractModuleNumber(m2)
    return n1 - n2
  })
}

function determineModuleName (fullFilePath) {
  if (fullFilePath.match(/.+reflektion\s\d+.+/i)) {
    return MODULE_TYPE.QUIZ
  }
  return MODULE_TYPE.PAGE
}

function extractQuestionList (moduleName, pageContent) {
  const regex = /^.+\?$/gm
  const array = pageContent.match(regex)
  if (array?.length > 0) {
    return array.map(q => (
      {
        'name': q,
        'questiontext': `<p>${q}</p>`,
        'quizName': moduleName,
        'qtype': 'essay',
      }
    ))
  }
  return []
}

async function extractModule (fileName, dir) {
  const moduleName = fileName.replace(/(.+)\.doc/, '$1')
  const fullFilePath = path.join(sourceDir, dir, fileName)
  const moduleType = determineModuleName(fullFilePath)
  const isQuiz = moduleType === MODULE_TYPE.QUIZ
  try {
    let data = ''
    if (fullFilePath.match(/.+\.rtf$/i)) {
      data = await convertToPlain(fullFilePath)
    } else {
      data = await reader.getText(fullFilePath)
    }
    return {
      'moduleName': moduleType,
      'sectionName': dir,
      ...(isQuiz ? {'quizName': moduleName} : {'pageName': moduleName}),
      'pageContent': data,
      'courseModuleType': moduleType.toUpperCase(),
      ...(isQuiz
        ? { 'questionList': extractQuestionList(moduleName, data) }
        : {}),
    }
  } catch (e) {
    console.error(`Could not parse ${fullFilePath}`, e)
    return {
      'moduleName': moduleType,
      'sectionName': dir,
      'pageName': moduleName,
      'pageContent': 'missing',
      ...(isQuiz ? { 'questionList': [] } : {}),
    }
  }
}

async function extractSections (sections) {

  const courseObj = {
    'course': {
      'fullname': COURSE.NAME,
      'summary': COURSE.SUMMARY,
    },
    'sectionList': [],
  }

  for (const dir of sections) {
    const name = dir.replace(/.+?-\s*/, '')
    const section = {
      name,
      courseModuleList: [],
    }
    const modules = fs.readdirSync(path.join(sourceDir, dir))
    const courseModuleList = []
    for (const fileName of modules) {
      courseModuleList.push(await extractModule(fileName, dir))
    }
    sortCourseModuleList(courseModuleList)
    section.courseModuleList = courseModuleList
    courseObj.sectionList.push(section)
  }

  return courseObj
}

const courseObj = await extractSections(sections)

fs.writeFileSync(path.join(targetDir, 'swedish_course.json'),
  JSON.stringify(courseObj, null, 2))




