import fetch from 'node-fetch';

async function listEntity(entityName) {
    const res = await fetch(`http://localhost:1337/${entityName}?_limit=1000`, {method: 'GET'});
    return res.json();
}

const listModules = async () => listEntity('modules');

const listSections = async () => listEntity('sections');

const listPages = async () => listEntity('pages');

const listQuizzes = async () => listEntity('quizzes');

const listQuestions = async () => listEntity('questions');

async function deleteEntities(ids, entityName) {
    return await Promise.all(ids.map(async id => {
        console.log(`Deleting ${entityName} `, id);
        return await fetch(`http://localhost:1337/${entityName}/${id}`, {method: 'DELETE'});
    }));
}

const deleteSections = async ids => deleteEntities(ids, 'sections');

const deleteModules = async ids => deleteEntities(ids, 'modules');

const deletePages = async ids => deleteEntities(ids, 'pages');

const deleteQuizzes = async ids => deleteEntities(ids, 'quizzes');

const deleteQuestions = async ids => deleteEntities(ids, 'questions');

listQuestions()
    .then(json => {
        return deleteQuestions(json.map(question => question.id));
    })
    .then(() => {
        return listQuizzes();
    })
    .then(json => {
        return deleteQuizzes(json.map(quiz => quiz.id));
    })
    .then(() => {
        return listPages();
    })
    .then(json => {
        const ids = json.map(page => page.id);
        return deletePages(ids);
    })
    .then(() => {
        return listModules();
    })
    .then(json => {
        const ids = json.map(module => module.id);
        return deleteModules(ids);
    })
    .then(res => {
        console.log(res);
        return listSections();
    })
    .then(sections => {
        const ids = sections.map(section => section.id);
        return deleteSections(ids);
    })
    .then(res => {
        console.log(res);
    });


