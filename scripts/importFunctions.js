const fetch = require('node-fetch')
const downloadFile = require('strapi-downloader')

const sectionLevel = 'section'
const moduleLevel = 'module'
const questionLevel = 'question'

let baseServer = ''
let sourceBase = ''

function setBaseServer (bs) {
  baseServer = bs
}

function setSourceBase (sb) {
  sourceBase = sb
}

async function createCourse (course, sectionIds) {
  const newCourse = {
    name: course.fullname,
    summary: course.summary,
    sections: sectionIds,
  }
  const res = await fetch(`${baseServer}/courses`,
    { method: 'POST', body: JSON.stringify(newCourse) })
  return await res.json()
}

reservedSections = ['INTRODUCTION']

async function createSection (section, moduleIds, ordinal) {
  let sectionName = reservedSections.includes[section.name]
    ? `${section.name} ${(new Date().getYear() + 1900)} `
    : section.name
  const newSection = {
    name: sectionName,
    accessible: true,
    modules: moduleIds,
    ordinal,
  }
  const res = await fetch(`${baseServer}/sections`,
    { method: 'POST', body: JSON.stringify(newSection) })
  const json = await res.json()
  return json.id
}

async function createModule (
  module, pageId, quizId, uploadedImage, uploadedAudio, uploadedVideo,
  ordinal) {
  const newModule = {
    name: module.moduleName,
    accessible: true,
    courseModuleType: module.courseModuleType,
    page: pageId,
    quiz: quizId,
    ordinal,
  }
  if (!!uploadedImage) {
    newModule.image = uploadedImage.id
  }
  if (!!uploadedAudio) {
    newModule.audio = uploadedAudio.id
  }
  if (!!uploadedVideo) {
    newModule.video = uploadedVideo.id
  }
  const res = await fetch(`${baseServer}/modules`,
    { method: 'POST', body: JSON.stringify(newModule) })
  const json = await res.json()
  return json.id
}

async function createPage (page) {
  const newPage = {
    pageName: page.pageName,
    pageContent: page.pageContent,
  }
  const res = await fetch(`${baseServer}/pages`,
    { method: 'POST', body: JSON.stringify(newPage) })
  const json = await res.json()
  if (json.statusCode && json.statusCode !== 200) {
    console.error('Could not create page', newPage)
  }
  return json.id
}

async function createQuiz (quiz, questionIds) {
  const newQuiz = {
    quizName: quiz.quizName,
    questions: questionIds,
  }
  const res = await fetch(`${baseServer}/quizzes`,
    { method: 'POST', body: JSON.stringify(newQuiz) })
  const json = await res.json()
  return json.id
}

async function createQuestion (question, ordinal) {
  const newQuestion = {
    name: question.name,
    questiontext: question.questiontext,
    qtype: question.qtype,
    ordinal: ordinal,
  }
  const res = await fetch(`${baseServer}/questions`,
    { method: 'POST', body: JSON.stringify(newQuestion) })
  try {
    const json = await res.json()
    return json.id
  } catch (e) {
    console.error('Exception whilst creating question', e)
  }
  return null
}

function extractUrl (module, type) {
  const item = module[type]
  if (!item) {
    return null
  }
  if(item.startsWith("http")) {
    return item
  }
  const url = item.url
  if (!url) {
    return null
  }
  if (type === 'image') {
    return `${sourceBase}${url}`
  } else {
    return url
  }
}

async function downloadItem (url) {
  let item = null
  if (!!url) {
    const json = await downloadFile.downloadFileToMediaFolder(url)
    item = json[0]
  }
  return item
}

async function navigate (entity, level = 'course') {
  switch (level) {
    case 'course':
      const ids = await navigate(entity.sectionList, sectionLevel)
      await createCourse(entity.course, ids)
      console.log('Course Created!', entity.course.name)
      break
    case sectionLevel: {
      const createdIds = []
      for (let i = 0; i < entity.length; i++) {
        const section = entity[i]
        const moduleIds = await navigate(section.courseModuleList, moduleLevel)
        createdIds.push(await createSection(section, moduleIds, i))
        console.log('Added section', section.name)
      }
      return createdIds
    }
    case moduleLevel: {
      const createdIds = []
      for (let i = 0; i < entity.length; i++) {
        let module = entity[i]
        let hasQuiz = !!module.quizName
        const pageId = !hasQuiz ? await createPage(module) : null
        const questionIds = hasQuiz ? await navigate(
          module.questionList, questionLevel) : []
        const quizId = hasQuiz
          ? await createQuiz(module, questionIds)
          : null
        const imageUrl = extractUrl(module, 'image')
        const uploadedImage = await downloadItem(imageUrl)
        const audioUrl = extractUrl(module, 'audio')
        const uploadedAudio = await downloadItem(audioUrl)
        const videoUrl = extractUrl(module, 'url')
        const uploadedVideo = await downloadItem(videoUrl)
        createdIds.push(
          await createModule(module, pageId, quizId, uploadedImage,
            uploadedAudio, uploadedVideo, i))
      }
      return createdIds
    }
    case questionLevel:
      const questionIds = []
      for (let i = 0; i < entity.length; i++) {
        let question = entity[i]
        questionIds.push(await createQuestion(question, i))
      }
      return questionIds
  }
}

module.exports = {
  setBaseServer,
  navigate,
  setSourceBase,
}