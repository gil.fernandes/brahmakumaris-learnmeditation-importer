const baseServer = process.env.STRAPI_BASE_SERVER || 'https://admin.learnmeditationonline.org';

module.exports = {
  baseServer
}