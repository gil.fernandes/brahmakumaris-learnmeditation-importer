import sys
from striprtf.striprtf import rtf_to_text

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Please specify the RTF file")
        sys.exit(1)
    with open(sys.argv[1]) as infile:
        content = infile.read()
    text = rtf_to_text(content)
    sys.stdout.buffer.write(text.encode('utf8'))

