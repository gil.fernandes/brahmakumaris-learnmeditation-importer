require('node-fetch')
const fs = require('fs')

// const baseServer = 'http://localhost:1338';
const baseServer = process.env.STRAPI_BASE_SERVER || 'https://admin.learnmeditationonline.org';

const { navigate, setBaseServer } = require('./importFunctions')

setBaseServer(baseServer)

const path = '../data/swedish_course.json'
if (fs.existsSync(path)) {
  const jsonStr = fs.readFileSync(path, {encoding:'utf8', flag:'r'})
  const json = JSON.parse(jsonStr)
  navigate(json).then(() => {
    console.log('Finished import')
    process.exit(0)
  })
} else {
  console.error(`File ${path} does not exist.`)
}

