

const baseServer = 'http://localhost:11337';
const sourceBase = 'http://www.learnmeditationonline.org';

let importFunctions = require('./importFunctions')
importFunctions.setBaseServer(baseServer)
importFunctions.setSourceBase(sourceBase)

const navigate = importFunctions.navigate

fetch(`${sourceBase}/elearning/course/sitemap.json`)
    .then(res => res.json())
    .then(json => navigate(json));