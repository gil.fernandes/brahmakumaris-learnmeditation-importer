const fetch = require('node-fetch');
const downloadFile = require('strapi-downloader');

// const baseServer = 'http://localhost:1338';
const baseServer = 'https://admin.learnmeditationonline.org';
const sourceBase = 'http://apprendreamediter.fr/';

const importFunctions = require('./importFunctions')
importFunctions.setBaseServer(baseServer)
importFunctions.setSourceBase(sourceBase)

const navigate = importFunctions.navigate

fetch(`${sourceBase}/elearning/course/sitemap.json`)
    .then(res => res.json())
    .then(json => navigate(json));

