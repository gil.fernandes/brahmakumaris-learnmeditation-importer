const fs = require('fs')
const fetch = require('node-fetch');
const FormData = require('form-data');
const { baseServer } = require('./baseServer')

const path = "../data/audios"
Symbol('buffer')
const readFileAsBlob = async (filePath) => {
  try {
    return fs.readFileSync(filePath)
  } catch (err) {
    console.error('Error reading file:', err);
    return null;
  }
};

async function uploadFile(form) {
  return await fetch(`${baseServer}/upload`, {
    method: 'POST',
    body: form
  });
}

const files = fs.readdirSync(path)
files.filter(f => f.endsWith(".mp3"))
async function processMp3s(files) {
  for(const file of files) {
    const blob = await readFileAsBlob(`${path}/${file}`)
    const form = new FormData();
    form.append('files', blob, {
      contentType: "audio/mp3",
      name: 'file',
      filename: file
    });
    const res = await uploadFile(form)
    console.log(res);
  }
  return files
}

processMp3s(files).then((files) => {
  console.log(`${files.length} files processed!`)
})