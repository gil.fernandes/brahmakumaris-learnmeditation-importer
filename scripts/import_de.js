const fetch = require('node-fetch');

const baseServer = 'https://admin.learnmeditationonline.org';
const sourceBase = 'http://www.online-meditieren-lernen.de';

let importFunctions = require('./importFunctions')
importFunctions.setBaseServer(baseServer)
importFunctions.setSourceBase(sourceBase)

const navigate = importFunctions.navigate

fetch(`${sourceBase}/elearning/course/sitemap.json`)
    .then(res => res.json())
    .then(json => navigate(json));

